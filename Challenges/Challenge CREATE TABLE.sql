--Voc� deve receber uma String que representa o nome de uma tabela
-- e retornar atrav�s de pesquisas no dicionario de dados
-- o CREATE TABLE da tabela informada


--You should receive a String that represents the name of a table
--and return through searches in the data dictionary
--the CREATE TABLE of the table reported

SELECT 
CASE  
    WHEN ROWNUM = 1 AND (SELECT CONSTRAINT_TYPE FROM USER_CONSTRAINTS WHERE TABLE_NAME = t.TABLE_NAME AND CONSTRAINT_TYPE = 'P' )= 'P' AND tc.nullable = 'N'  THEN 
    'CREATE TABLE ' ||t.table_name||'('||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||') '||'Primary Key'
    WHEN ROWNUM = 1 AND tc.nullable ='Y' THEN
    'CREATE TABLE ' ||t.table_name||'('||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||')'
    WHEN tc.nullable = 'N' AND tc.COLUMN_ID =1 THEN
    LPAD(','||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||') Primary Key',length('CREATE TABLE ' ||t.table_name||'('||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||')')+13, ' ')   
    WHEN tc.nullable = 'N' THEN
    LPAD(','||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||') NOT NULL',length('CREATE TABLE ' ||t.table_name||'('||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||')')+10, ' ')   
    ELSE
    LPAD(','||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||')',length('CREATE TABLE ' ||t.table_name||'('||tc.column_name ||' '||tc.DATA_TYPE||'('||tc.DATA_LENGTH||')')+1, ' ')   
    END || CHR(13)||
    CASE WHEN ROWNUM=(SELECT COUNT(*)COLUMN_NAME FROM USER_TAB_COLS WHERE TABLE_NAME = t.table_name)
    THEN
     REPLACE(');','','') 
   END 
 FROM user_tables      t
     ,user_tab_columns tc
WHERE t.table_name = tc.table_name
AND t.table_name = UPPER('&TABELA');


SELECT *FROM user_tab_columns WHERE TABLE_NAME = 'EMPLOYEES';

SELECT * FROM USER_CONSTRAINTS WHERE TABLE_NAME = '';    



SELECT NVL(INDEX_NAME,0) FROM USER_CONSTRAINTS WHERE TABLE_NAME = 'EMPLOYEES' AND INDEX_NAME != '0';