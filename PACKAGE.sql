SET Serveroutput On
SELECT * FROM USER_TABLES;
SELECT * FROM SYS.User_Tab_Cols;



CREATE OR REPLACE PACKAGE BODY pkg_estags AS
  PROCEDURE renomearTodasTabelas
  is
  CURSOR c_renomear 
  IS
    SELECT Table_Name FROM USER_TABLES;
    nm_table USER_TABLES.Table_Name%TYPE;
    sql_stmnt varchar2(400) ;
  BEGIN
  FOR rec_name IN c_renomear
  LOOP
    nm_table := REPLACE(rec_name,'T_AULA1_','');
    sql_stmnt := 'ALTER TABLE'|| rec_name ||'RENAME TO'||
    nm_table;
    Dbms_Output.Put_Line(sql_stmnt);
    Execute immediate sql_stmnt;
  END LOOP ;
  
  end renomearTodasTabelas;
end pkg_estags;

DECLARE
BEGIN 
  pkg_estags.renomearTodasTabelas;
END;

  --FUNCTION retornaDataVencimento(p_nr_dias_vencimento T_A)